package gr.hua.dit.it21685.androidassignment

import android.text.Editable
import android.text.TextWatcher

/**
 * [TextWatcher] that validates text using a [Regex].
 *
 * @param regex the valid regex pattern.
 */
abstract class TextValidator(regex: String) : TextWatcher {
    /** The regex pattern converted to a [Regex] */
    internal val regex = regex.toRegex()

    /**
     * This method is used in [afterTextChanged] to
     * validate the [text] of the [Editable].
     *
     * @param text the input text to be validated.
     */
    abstract fun validate(text: String)

    override fun afterTextChanged(s: Editable) { validate(s.toString()) }

    override fun beforeTextChanged(c: CharSequence, s: Int, n: Int, a: Int) {}

    override fun onTextChanged(c: CharSequence, s: Int, b: Int, n: Int) {}
}

