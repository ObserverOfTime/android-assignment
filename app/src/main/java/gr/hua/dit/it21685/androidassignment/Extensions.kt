package gr.hua.dit.it21685.androidassignment

import android.app.Activity
import android.support.v7.app.AlertDialog
import android.widget.Toast
import kotlinx.android.synthetic.main.error_alert.*

/** Converts the [CharSequence] to a [Float] number or 0 if null. */
internal fun CharSequence?.toFloat() = toString().toFloatOrNull() ?: 0F

/**
 * Creates a toast with the specified [message].
 *
 * @param message the message to display.
 * @param long controls whether the toast should be
 *             [Toast.LENGTH_LONG] or [Toast.LENGTH_SHORT].
 */
internal fun Activity.toast(message: String, long: Boolean = false) {
    val duration = if (long) Toast.LENGTH_LONG else Toast.LENGTH_SHORT
    Toast.makeText(this, message, duration).show()
}

/**
 * Creates an error modal with the specified [message].
 *
 * @param message the error message.
 */
internal fun Activity.error(message: String) {
    AlertDialog.Builder(this)
            .setView(R.layout.error_alert).create().apply {
                setOnShowListener {
                    errorAlertText.text = message
                    errorAlertOKBtn.setOnClickListener { dismiss() }
                }
                show()
            }
}

