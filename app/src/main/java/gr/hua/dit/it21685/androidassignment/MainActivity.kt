package gr.hua.dit.it21685.androidassignment

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

/** The main activity of the application. */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        instance = this
        val fields = mutableListOf(
                userFieldLayout,
                timestampFieldLayout,
                longitudeFieldLayout,
                latitudeFieldLayout
        )
        with(userFieldLayout) {
            editText!!.addTextChangedListener(
                    object : TextValidator("^.{1,10}$") {
                        override fun validate(text: String) {
                            error = when {
                                text.isBlank() -> "Cannot be empty!"
                                text.length > 10 -> "No more than 10 letters!"
                                else -> null
                            }
                        }
                    })
            requestFocus()
        }
        with(timestampFieldLayout) {
            val pattern = "^\\d{4}(-\\d{2}){2} \\d{2}(:\\d{2}){2}$"
            editText!!.setText(getCurrentTimeStamp())
            editText!!.addTextChangedListener(
                    object : TextValidator(pattern) {
                        override fun validate(text: String) {
                            error = if (regex.matches(text)) null else
                                "Must match '$TIMESTAMP_FORMAT'!"
                        }
                    }
            )
        }
        // Longitude & latitude fields
        fields.slice(2..3).forEach {
            it.editText!!.addTextChangedListener(
                    object : TextValidator("^-?\\d+([.,]\\d+)?$") {
                        override fun validate(text: String) {
                            it.error = "Must be a valid number!".takeUnless {
                                regex.matches(text)
                            }
                        }
                    })
        }
        insertBtn.setOnClickListener {
            when {
                // Display an error if any field is invalid
                fields.any { f -> !f.error.isNullOrBlank() } -> {
                    error(getString(R.string.fieldError))
                }
                // A different error if any field is blank
                fields.any { f -> f.editText!!.text.isNullOrBlank() } -> {
                    error(getString(R.string.fieldMissing))
                }
                else -> {
                    val location = Location(
                            user = userInsertField.text.toString(),
                            longitude = longitudeInsertField.text.toFloat(),
                            latitude = latitudeInsertField.text.toFloat(),
                            timestamp = timestampInsertField.text.toString()
                    )
                    try {
                        // Try to insert the location
                        DBHelper.insert(location)
                        Log.v("Inserted location", location.toString())
                        toast(getString(R.string.locationAdded))
                    } catch (ex: Throwable) {
                        // Show the exception if inserting failed
                        error(ex.message ?: "Whoops! Something went wrong.")
                        Log.e("Insert failed", location.toString(), ex)
                    }
                }
            }
        }
        selectBtn.setOnClickListener {
            startActivity(Intent(
                    this@MainActivity,
                    TimestampPickerActivity::class.java
            ))
        }
        viewAllBtn.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW).apply {
                type = "$MIME_TYPE_ID.locations"
            })
        }
    }
    /**
     * Get the current timestamp as [TIMESTAMP_FORMAT].
     *
     * @return the formatted timestamp.
     */
    private fun getCurrentTimeStamp(): String {
        return SimpleDateFormat(TIMESTAMP_FORMAT, Locale.getDefault())
                .format(Calendar.getInstance().time)
    }

    companion object {
        /** The instance of the activity. */
        lateinit var instance: MainActivity
            private set

        /** The default timestamp format of the app. */
        private const val TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss"

        /** Mime type ID from [BuildConfig.APPLICATION_ID]. */
        private const val MIME_TYPE_ID =
                "application/vnd.${BuildConfig.APPLICATION_ID}"
    }
}

