package gr.hua.dit.it21685.androidassignment

import android.content.ContentValues
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/** The version of the database. */
private const val DB_VERSION = 1

/** The name of the database. */
private const val DB_NAME = "locations.db"

/** The name of the table. */
private const val TABLE_NAME = "LOCATIONS"

/** Converts a [Cursor] object into a [Location]. */
private fun Cursor.toLocation() = Location(
        id = getInt(0),
        user = getString(1),
        longitude = getFloat(2),
        latitude = getFloat(3),
        timestamp = getString(4)
)

/** The database helper of the application. */
object DBHelper : SQLiteOpenHelper(
        MainActivity.instance, DB_NAME, null, DB_VERSION) {

    /**
     * Enum representing the columns of the [table][TABLE_NAME].
     *
     * @property value the name of the column.
     * @property definition the definition of the column.
     */
    enum class Column(val value: String, private val definition: String) {
        /** The id column. */
        ID("id", "INTEGER PRIMARY KEY AUTOINCREMENT"),

        /** The user ID column. */
        USER("userid", "TEXT NOT NULL"),

        /** The longitude column. */
        LONGITUDE("longitude", "FLOAT NOT NULL"),

        /** The latitude column. */
        LATITUDE("latitude", "FLOAT NOT NULL"),

        /** The timestamp column. */
        TIMESTAMP("dt", "TEXT NOT NULL");

        override fun toString() = value

        companion object {
            /** Defines the table columns. */
            internal fun define(): String {
                return values().joinToString(prefix = "(", postfix = ")") {
                    "`${it.value}` ${it.definition}"
                }
            }
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("CREATE TABLE `$TABLE_NAME` ${Column.define()};")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVer: Int, newVer: Int) {}

    /**
     * Inserts a new record into the [table][TABLE_NAME].
     *
     * @param location a [Location] object to be inserted.
     * @return the row ID of the inserted record.
     * @throws [SQLException] if an error occurred.
     */
    @Throws(SQLException::class)
    fun insert(location: Location) = writableDatabase.insertOrThrow(
            TABLE_NAME, null,
            ContentValues().apply {
                put(Column.USER.value, location.user)
                put(Column.LONGITUDE.value, location.longitude)
                put(Column.LATITUDE.value, location.latitude)
                put(Column.TIMESTAMP.value, location.timestamp)
            }
    )

    /**
     * Performs a select query in the database.
     *
     * @see [SQLiteDatabase.query]
     */
    private fun select(columns: Array<String> = arrayOf("*"),
                       selection: String? = null,
                       selectionArgs: Array<String>? = null,
                       groupBy: String? = null,
                       having: String? = null,
                       orderBy: String? = null,
                       limit: String? = null,
                       distinct: Boolean = false): Cursor {
        return readableDatabase.query(
                distinct, TABLE_NAME, columns,
                selection, selectionArgs, groupBy,
                having, orderBy, limit
        )
    }

    /**
     * Get a list of locations by [userId] and [timestamp].
     *
     * @param [userId] the user ID of the locations.
     * @param [timestamp] the timestamp of the locations.
     * @return the list of locations.
     */
    fun getLocations(userId: String, timestamp: String): List<Location> {
        val locations = mutableListOf<Location>()
        select(selection = "${Column.USER}=? AND ${Column.TIMESTAMP}=?",
                selectionArgs = arrayOf(userId, timestamp)).use {
            while (it.moveToNext()) locations.add(it.toLocation())
        }
        return locations
    }

    /**
     * Get a distinct list of all entries in a certain [Column].
     *
     * @param column the column to retrieve.
     * @return the list of distinct entries.
     */
    fun getDistinct(column: Column): List<String> {
        val entries = mutableListOf<String>()
        select(columns = arrayOf(column.value),
                orderBy = column.value, distinct = true).use {
            while (it.moveToNext()) entries.add(it.getString(0))
        }
        return entries
    }

    /**
     * Get a list of all the locations in the database.
     *
     * @return the full list of locations.
     */
    fun allLocations(): List<Location> = mutableListOf<Location>().apply {
        select().use { while (it.moveToNext()) add(it.toLocation()) }
    }
}

