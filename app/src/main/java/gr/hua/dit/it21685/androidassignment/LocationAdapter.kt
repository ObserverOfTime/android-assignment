package gr.hua.dit.it21685.androidassignment

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import kotlinx.android.synthetic.main.location_item.view.*

/**
 * Adapter which populates a [ListView][android.widget.ListView]
 * from a list of locations.
 *
 * @property activity the activity where the list will be displayed.
 * @property locations the list of locations to display.
 */
class LocationAdapter(private val activity: Activity,
                      private val locations: List<Location>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?,
                         parent: ViewGroup?): View {
        val holder: ViewHolder
        val view: View

        if (convertView == null) {
            holder = ViewHolder()
            view = activity.layoutInflater.inflate(
                    R.layout.location_item, parent, false
            )
            holder.user = view.userCell
            holder.timestamp = view.timestampCell
            holder.latitude = view.latitudeCell
            holder.longitude = view.longitudeCell
            view.tag = holder
        } else {
            view = convertView
            holder = convertView.tag as ViewHolder
        }

        val location = getItem(position)
        holder.user.text = location.user
        holder.timestamp.text = location.timestamp
        holder.latitude.text = location.latitude.toString()
        holder.longitude.text = location.longitude.toString()

        return view
    }

    override fun getItem(position: Int) = locations[position]

    override fun getItemId(position: Int) =
            locations[position].id.toLong()

    override fun getCount() = locations.size

    /** [Location] row view holder. */
    inner class ViewHolder {
        /** The [Location.user] cell. */
        lateinit var user: TextView

        /** The [Location.timestamp] cell. */
        lateinit var timestamp: TextView

        /** The [Location.latitude] cell. */
        lateinit var latitude: TextView

        /** The [Location.longitude] cell */
        lateinit var longitude: TextView
    }
}

