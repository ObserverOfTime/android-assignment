package gr.hua.dit.it21685.androidassignment

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_location_list.*

class LocationListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_list)
        locationList.adapter = LocationAdapter(
                this@LocationListActivity,
                DBHelper.getLocations(
                        intent.getStringExtra("userId"),
                        intent.getStringExtra("timestamp")
                )
        )
        locationList.setOnItemClickListener { par, view, pos, id ->
            val location = locationList.adapter.getItem(pos) as Location
            toast("User ID: ${location.user}\n" +
                    "Timestamp: ${location.timestamp}\n" +
                    "Longitude: ${location.longitude}\n" +
                    "Latitude: ${location.latitude}", true)
        }
    }
}

// Based on http://www.technotalkative.com/android-multi-column-listview/

