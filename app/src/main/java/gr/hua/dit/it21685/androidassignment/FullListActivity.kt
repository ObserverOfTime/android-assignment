package gr.hua.dit.it21685.androidassignment

import android.app.Activity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_full_list.*

class FullListActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_list)
        fullList.adapter = LocationAdapter(
                this@FullListActivity,
                DBHelper.allLocations()
        )
        fullList.setOnItemClickListener { par, view, pos, id ->
            val location = fullList.adapter.getItem(pos) as Location
            toast("User ID: ${location.user}\n" +
                    "Timestamp: ${location.timestamp}\n" +
                    "Longitude: ${location.longitude}\n" +
                    "Latitude: ${location.latitude}", true)
        }
    }
}
