package gr.hua.dit.it21685.androidassignment

import android.R.layout.simple_dropdown_item_1line
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_timestamp_list.*

class TimestampPickerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timestamp_list)
        userSearchField.setAdapter(ArrayAdapter<String>(
                this@TimestampPickerActivity,
                simple_dropdown_item_1line,
                DBHelper.getDistinct(DBHelper.Column.USER)
        ))
        timestampSearchList.adapter = ArrayAdapter<String>(
                this@TimestampPickerActivity,
                R.layout.dropdown_item,
                DBHelper.getDistinct(DBHelper.Column.TIMESTAMP)
        ).also { it.setDropDownViewResource(simple_dropdown_item_1line) }
        viewBtn.setOnClickListener {
            if (userSearchField.text.isNullOrBlank()) {
                userSearchField.error = "Please enter a user ID."
            } else {
                userSearchField.error = null
                startActivity(Intent(
                        this@TimestampPickerActivity,
                        LocationListActivity::class.java
                ).apply {
                    putExtra("userId",
                            userSearchField.text.toString())
                    putExtra("timestamp",
                            timestampSearchList.selectedItem.toString())
                })
            }
        }
    }
}

